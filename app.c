#include <stdio.h>
#include <stdlib.h>

#define UNKNOWN 0
#define BLOCK 1
#define EMPTY 2
#define GRID_SIZE 25
#define ROW 1
#define COL 2

struct GridSpan 
{
	int pos;
	int length;
	int locked;
	struct GridSpan* next;
	struct GridSpan* prev;
};



void setKnownGridPoints(void);
void createElements(const int *input, unsigned long int inputSize, struct GridSpan **elementsByIndex);

void printGrid(void);
void printElements(struct GridSpan **elementsByRow);

void cleanUp(void);

int getGridValue(int direction, int index, int pos);
struct GridSpan* getLastSpan(struct GridSpan *elements);
struct GridSpan* getLastLockedSpanForward(struct GridSpan *elements);
struct GridSpan* getLastLockedSpanReverse(struct GridSpan *elements);
struct GridSpan* getLargestUnlockedSpan(struct GridSpan *elements);
int getLengthToEnd(struct GridSpan *startSpan);
int getLeftSpanStart(struct GridSpan *elements, struct GridSpan *span);
int getRightSpanStart(struct GridSpan *elements, struct GridSpan *span);
struct GridSpan getPerpendicularSpan(int direction, int index, int pos);
int checkSpanFits(int direction, int index, struct GridSpan span);
int checkRemainingSpansFit(int direction, int index, struct GridSpan *startSpan, int startPos);

void calculateSolution(void);
int searchDefaultLayoutForward(int direction, int index, struct GridSpan *startSpan, int startPos);
int searchDefaultLayoutReverse(int direction, int index, struct GridSpan *startSpan, int startPos);
int calculatePossibleLayouts(int direction, int index, struct GridSpan *elements);
int searchForward(int direction, int index, struct GridSpan *elements);
int searchReverse(int direction, int index, struct GridSpan *elements);
int searchMiddle(int direction, int index, struct GridSpan *elements);
int searchSpansInBlocks(int direction, int index, struct GridSpan *elements);
int searchBlocksInSpans(int direction, int index, struct GridSpan *elements);

void fillInBlanks(int direction, int index, struct GridSpan *elements);
void lockInElements(int direction, int index, struct GridSpan *elements);



const int horizontalInput[182] = { 7,3,1,1,7,-1,1,1,2,2,1,1,-1,1,3,1,3,1,1,3,1,-1,1,3,1,1,6,1,3,1,-1,1,3,1,5,2,1,3,1,-1,1,1,2,1,1,-1,7,1,1,1,1,1,7,-1,3,3,-1,1,2,3,1,1,3,1,1,2,-1,1,1,3,2,1,1,-1,4,1,4,2,1,2,-1,1,1,1,1,1,4,1,3,-1,2,1,1,1,2,5,-1,3,2,2,6,3,1,-1,1,9,1,1,2,1,-1,2,1,2,2,3,1,-1,3,1,1,1,1,5,1,-1,1,2,2,5,-1,7,1,2,1,1,1,3,-1,1,1,2,1,2,2,1,-1,1,3,1,4,5,1,-1,1,3,1,3,10,2,-1,1,3,1,1,6,6,-1,1,1,2,1,1,2,-1,7,2,1,2,5,-1 };
const int verticalInput[186] = {7,2,1,1,7,-1,1,1,2,2,1,1,-1,1,3,1,3,1,3,1,3,1,-1,1,3,1,1,5,1,3,1,-1,1,3,1,1,4,1,3,1,-1,1,1,1,2,1,1,-1,7,1,1,1,1,1,7,-1,1,1,3,-1,2,1,2,1,8,2,1,-1,2,2,1,2,1,1,1,2,-1,1,7,3,2,1,-1,1,2,3,1,1,1,1,1,-1,4,1,1,2,6,-1,3,3,1,1,1,3,1,-1,1,2,5,2,2,-1,2,2,1,1,1,1,1,2,1,-1,1,3,3,2,1,8,1,-1,6,2,1,-1,7,1,4,1,1,3,-1,1,1,1,1,4,-1,1,3,1,3,7,1,-1,1,3,1,1,1,2,1,1,4,-1,1,3,1,4,3,3,-1,1,1,2,2,2,6,1,-1,7,1,3,2,1,1,-1};
const unsigned long int	horizontalInputLen = sizeof(horizontalInput) / sizeof(int);
const unsigned long int	verticalInputLen = sizeof(verticalInput) / sizeof(int);

struct GridSpan *horizontalElementsByRow[GRID_SIZE];
struct GridSpan *verticalElementsByColumn[GRID_SIZE];


/*
	Final grid is row, col
*/
char finalGrid[GRID_SIZE][GRID_SIZE] = {};


int main (void)
{
	printf ("horizontal elements %lu\n", horizontalInputLen);
	printf ("vertical elements %lu\n", verticalInputLen);
	createElements(horizontalInput, horizontalInputLen, horizontalElementsByRow);
	createElements(verticalInput, verticalInputLen, verticalElementsByColumn);
	setKnownGridPoints();

	//printElements(horizontalElementsByRow);
	//printElements(verticalElementsByColumn);
	calculateSolution();
	printGrid();
	cleanUp();
  	return 0;
}

void cleanUp(void) 
{
	struct GridSpan *currentElement, *nextElement;
	for(int i=0; i<GRID_SIZE; i++) {
		currentElement = horizontalElementsByRow[i];

		while(currentElement != NULL) {
			nextElement = currentElement->next;
			free(currentElement);
			currentElement = nextElement;
		}
	}

	for(int i=0; i<GRID_SIZE; i++) {
		currentElement = verticalElementsByColumn[i];

		while(currentElement != NULL) {
			nextElement = currentElement->next;
			free(currentElement);
			currentElement = nextElement;
		}
	}
}


// SETUP
// -----
void createElements(const int *input, unsigned long int inputSize, struct GridSpan **elementsByIndex)
{
	// Figure out size
	int count = 0;
	for(unsigned long int i=0; i < inputSize; i++) {
		if(input[i] != -1) {
			count++;
		}
	}


	int index = 0;
	struct GridSpan *lastElement = NULL;

	for(unsigned long int i=0; i < inputSize; i++) {
		if(input[i] != -1) {
			struct GridSpan *newElement = (struct GridSpan*)malloc(sizeof(struct GridSpan));
			if(newElement == NULL)
		    {
		        printf("Error creating a new element.\n");
		        exit(0);
		    }

			newElement->length = input[i];
			newElement->locked = 0;
			newElement->pos = -1;
			newElement->next = NULL;
			newElement->prev = NULL;

			if(lastElement == NULL) {
				lastElement = newElement;
				elementsByIndex[index] = newElement;
			} else {
				lastElement->next = newElement;
				newElement->prev = lastElement;
				lastElement = newElement;
			}

		} else {
			index++;
			lastElement = NULL;
		}
	}
}

void setKnownGridPoints(void)
{
	// row 3
	finalGrid[3][3] = BLOCK;
	finalGrid[3][4] = BLOCK;
	finalGrid[3][12] = BLOCK;
	finalGrid[3][13] = BLOCK;
	finalGrid[3][21] = BLOCK;

	// row 8
	finalGrid[8][6] = BLOCK;
	finalGrid[8][7] = BLOCK;
	finalGrid[8][10] = BLOCK;
	finalGrid[8][14] = BLOCK;
	finalGrid[8][15] = BLOCK;
	finalGrid[8][18] = BLOCK;

	// row 16
	finalGrid[16][6] = BLOCK;
	finalGrid[16][11] = BLOCK;
	finalGrid[16][16] = BLOCK;
	finalGrid[16][20] = BLOCK;

	// row 21
	finalGrid[21][3] = BLOCK;
	finalGrid[21][4] = BLOCK;
	finalGrid[21][9] = BLOCK;
	finalGrid[21][10] = BLOCK;
	finalGrid[21][15] = BLOCK;
	finalGrid[21][20] = BLOCK;
	finalGrid[21][21] = BLOCK;
}


// PRINT FUNCTIONS
// ---------------

void printGrid(void) 
{
	printf("   ");
	for(int col = 0;col<GRID_SIZE; col++)
	{
		printf("%i ", col%10);
	}
	printf("\n");

	for(int row = 0;row<GRID_SIZE; row++) 
	{
		printf("%i] ", row % 10);
		for(int col = 0;col<GRID_SIZE; col++)
		{
			//printf ("%i", );
			if(finalGrid[row][col] == BLOCK) 
			{
				printf("X ");
			} 
			else if(finalGrid[row][col] == EMPTY) 
			{
				printf("  ");
			} 
			else 
			{
				printf("%c ", 45 );
			}
		}
		printf("\n");
	}
}

void printElements(struct GridSpan **elementsByRow)
{
	struct GridSpan *currentElement;
	for(int i=0; i<GRID_SIZE; i++) {

		printf("%i - ", i);
		currentElement = elementsByRow[i];

		while(currentElement != NULL) {
			printf("len: %i lock: %i, ", currentElement->length, currentElement->locked);
			currentElement = currentElement->next;
		}
		printf("\n");
	}
}

// CALC
// ----


// MAIN
void calculateSolution(void)
{

	for(int row = 0;row<GRID_SIZE; row++) 
	{
		if(searchDefaultLayoutForward(ROW, row, horizontalElementsByRow[row], 0)) {
			lockInElements(ROW, row, horizontalElementsByRow[row]);
		}
	}

	for(int col = 0;col<GRID_SIZE; col++) 
	{
		if(searchDefaultLayoutForward(COL, col, verticalElementsByColumn[col], 0)) {
			lockInElements(COL, col, verticalElementsByColumn[col]);
		}
	}

	int solvedCount = 0, iteration = 0;
	while(solvedCount != (GRID_SIZE + GRID_SIZE)) {
		// if(iteration == 4) {
		// 	return;
		// }

		iteration++;
		solvedCount = 0;

		printf("==========================\n");
		printf("==========================\n");
		printf("Iteration %i\n", iteration);
		printf("==========================\n");
		printf("ROWS\n==========================\n\n");

		for(int row = 0;row<GRID_SIZE; row++) 
		{
			int results = calculatePossibleLayouts(ROW, row, horizontalElementsByRow[row]);
			//printf("row %i == results %i\n", row, results);

			if(results == 0) {
				solvedCount++;
			} else if(results == 1) {
				// Only one possible layout so lock in solution
				lockInElements(ROW, row, horizontalElementsByRow[row]);
			}
			fillInBlanks(ROW, row, horizontalElementsByRow[row]);
			printf("\n\n");
		}

		printf("==========================\n");
		printf("COLS\n==========================\n\n");
		for(int col = 0;col<GRID_SIZE; col++) 
		{
			int results = calculatePossibleLayouts(COL, col, verticalElementsByColumn[col]);
			//printf("row %i == results %i\n", row, results);

			if(results == 0) {
				solvedCount++;
			} else if(results == 1) {
				// Only one possible layout so lock in solution
				lockInElements(COL, col, verticalElementsByColumn[col]);
			}
			fillInBlanks(COL, col, verticalElementsByColumn[col]);
			printf("\n\n");
		}

		//printGrid();
	}

	printf("Took %i iterations to solve\n", iteration);
}


int getGridValue(int direction, int index, int pos) 
{
	if(direction == ROW) {
		return finalGrid[index][pos];
	}

	return finalGrid[pos][index];
}

int searchDefaultLayoutForward(int direction, int index, struct GridSpan *startSpan, int startPos)
{
	printf("%i) Search Default Forward\n", index);
	printf("- start pos %i\n", startPos);
	int maxLength = startPos, pos = startPos;
	struct GridSpan *currentSpan = startSpan;
	while (currentSpan != NULL) {
		maxLength += currentSpan->length;
		currentSpan = currentSpan->next;

		// Add required gap
		if(currentSpan != NULL) {
			maxLength += 1;
		}
	}

	// Elements default across whole grid
	if(maxLength == GRID_SIZE) {
		pos = startPos;
		currentSpan = startSpan;
		while (currentSpan != NULL) {
			currentSpan->pos = pos;
			currentSpan->locked = 1;

			pos += currentSpan->length + 1;
			currentSpan = currentSpan->next;
		}
		return 1;
	}

	return 0;
}

int searchDefaultLayoutReverse(int direction, int index, struct GridSpan *startSpan, int startPos)
{
	printf("%i) Search Default Reverse\n", index);
	printf("- start pos %i\n", startPos);

	int endPos = startPos, pos = 0;
	struct GridSpan *currentSpan = startSpan;
	while (currentSpan != NULL) {
		endPos -= (currentSpan->length - 1);
		currentSpan = currentSpan->prev;

		// Add required gap
		if(currentSpan != NULL) {
			endPos -= 1;
		}
	}

	// Elements default across whole grid
	if(endPos == 0) {
		pos = startPos;
		currentSpan = startSpan;
		while (currentSpan != NULL) {
			currentSpan->pos = pos - (currentSpan->length - 1);
			currentSpan->locked = 1;

			pos -= 1;
			currentSpan = currentSpan->prev;
		}
		return 1;
	}
	return 0;
}

int calculatePossibleLayouts(int direction, int index, struct GridSpan *elements)
{
	int maxLength = 0, elementCount = 0, lockedCount = 0;
	struct GridSpan *currentSpan = elements;
	while (currentSpan != NULL) {

		elementCount++;
		lockedCount += currentSpan->locked ? 1 : 0;
		maxLength += currentSpan->length;
		currentSpan = currentSpan->next;

		// Add required gap
		if(currentSpan != NULL) {
			maxLength += 1;
		}
	}

	// Already solved?
	if(elementCount == lockedCount) {
		return 0;
	}


	if(searchForward(direction, index, elements) == 1 || searchReverse(direction, index, elements) == 1
	 || searchMiddle(direction, index, elements) == 1 
	 //|| searchSpansInBlocks(direction, index, elements) == 1
	  || searchBlocksInSpans(direction, index, elements) == 1
	 ) {
		return 1;
	}

	return 0;
}

int getLengthToEnd(struct GridSpan *startSpan) 
{
	int count = 0;
	while(startSpan != NULL) {
		if(startSpan->locked) {
			count = startSpan->pos;
		}
		count += startSpan->length + (startSpan->next == NULL ? 0 : 1);
		startSpan = startSpan->next;
	}
	return count;
}

int getLeftSpanStart(struct GridSpan *elements, struct GridSpan *span)
{
	if(span->locked) {
		return span->pos;
	}

	int leftPos = 0;
	struct GridSpan *currentSpan = elements;
	while ((currentSpan != NULL) && (currentSpan != span)) {

		if(currentSpan->locked) {
			leftPos = currentSpan->pos;
		}

		leftPos += currentSpan->length + 1;
		currentSpan = currentSpan->next;
	}
	return leftPos;
}

int getRightSpanStart(struct GridSpan *elements, struct GridSpan *span)
{
	if(span->locked) {
		return span->pos + span->length - 1;
	}

	int rightPos = GRID_SIZE - 1;
	struct GridSpan *currentSpan = getLastSpan(elements);
	while ((currentSpan != NULL) && (currentSpan != span)) {
		if(currentSpan->locked) {
			rightPos = currentSpan->pos - 2;
		} else {
			rightPos -= currentSpan->length + 1;
		}

		currentSpan = currentSpan->prev;
	} 
	return rightPos;
}

struct GridSpan getPerpendicularSpan(int direction, int index, int pos) 
{
	
	int i = 0;
	direction = direction == ROW ? COL : ROW;

	struct GridSpan result;
	result.length = 1;
	result.pos = index;

	i = index - 1;
	while (i >= 0) {
		if(getGridValue(direction, pos, i) == BLOCK) {
			result.pos--;
			result.length++;
		} else {
			break;
		}
		i--;
	}

	i = index + 1;
	while (i < GRID_SIZE) {
		if(getGridValue(direction, pos, i) == BLOCK) {
			result.length++;
		} else {
			break;
		}
		i++;
	}

	return result;
}

int checkSpanFits(int direction, int index, struct GridSpan span)
{
	int leftPos = 0, rightPos = 0;
	struct GridSpan *currentSpan;
	struct GridSpan *elements;
	if(direction == ROW) {
		elements = horizontalElementsByRow[index];
	} else {
		elements = verticalElementsByColumn[index];
	}

	currentSpan = elements;
	while (currentSpan != NULL) {
		leftPos = getLeftSpanStart(elements, currentSpan);
		rightPos = getRightSpanStart(elements, currentSpan);

		if((leftPos <= span.pos) && (rightPos >= (span.pos + (span.length - 1))) && currentSpan->length >= span.length) {
			return 1;
		}

		currentSpan = currentSpan->next;
	}

	return 0;
}

int checkRemainingSpansFit(int direction, int index, struct GridSpan *startSpan, int startPos)
{
	int i = 0, consecutiveBlocks = 0;
	struct GridSpan *currentSpan;

	if(startSpan == NULL) {
		return 1;
	}

	// Count up expected 
	currentSpan = startSpan;
	while (currentSpan != NULL) {
		if(currentSpan->locked == 1) {
			startPos = currentSpan->pos + currentSpan->length + 1;
			currentSpan = currentSpan->next;
			continue;
		}


		consecutiveBlocks = 0;
		for( i = startPos; i < GRID_SIZE; i++ ) {
			if(getGridValue(direction, index, i) == EMPTY) {
				if(i == (GRID_SIZE - 1)) {
					// Doesn't fit
					return 0;
				}
				consecutiveBlocks = 0;
				continue;
			}

			consecutiveBlocks++;
			if(consecutiveBlocks == currentSpan->length) {
				// Fits
				startPos = i + 1;
				break;
			} else if(i == (GRID_SIZE - 1)) {
				// Doesn't fit
				return 0;
			}
		}

		currentSpan = currentSpan->next;
	}

	return 1;
}

struct GridSpan* getLargestUnlockedSpan(struct GridSpan *elements) {
	struct GridSpan *currentSpan = elements;
	struct GridSpan *largestSpan = NULL;
	while (currentSpan != NULL) {
		if(!currentSpan->locked && ((largestSpan == NULL) || (currentSpan->length > largestSpan->length))) {
			largestSpan = currentSpan;
		}
		currentSpan = currentSpan->next;
	}
	return largestSpan;
}

struct GridSpan* getLastSpan(struct GridSpan *elements)
{
	struct GridSpan *currentSpan = elements;
	struct GridSpan *lastSpan = NULL;
	while (1) {
		if(currentSpan->next == NULL) {
			lastSpan = currentSpan;
			break;
		}

		currentSpan = currentSpan->next;
	}
	return lastSpan;
}

struct GridSpan* getLastLockedSpanForward(struct GridSpan *elements)
{
	struct GridSpan *currentSpan = elements;
	struct GridSpan *lastLockedSpan = NULL;

	while (currentSpan != NULL) {
		if(!currentSpan->locked) {
			break;
		}
		lastLockedSpan = currentSpan;
		currentSpan = currentSpan->next;
	}

	return lastLockedSpan;
}

struct GridSpan* getLastLockedSpanReverse(struct GridSpan *elements)
{
	struct GridSpan *currentSpan = getLastSpan(elements);
	struct GridSpan *lastLockedSpan = NULL;

	while (currentSpan != NULL) {
		if(!currentSpan->locked) {
			break;
		}
		lastLockedSpan = currentSpan;
		currentSpan = currentSpan->prev;
	}

	return lastLockedSpan;
}

int searchForward(int direction, int index, struct GridSpan *elements)
{

	int pos = 0, gridValue = 0, consecutiveBlocks = 0, consecutiveUnknown = 0, found = 0;
	struct GridSpan *currentSpan;

	printf("%i) Search Forward\n", index);
	
	// Find first span that is locked
	currentSpan = getLastLockedSpanForward(elements);

	if(currentSpan == NULL) {
		// No locked span yet
		pos = 0;
		currentSpan = elements;
	} else if(currentSpan->next == NULL) {
		// All span are locked
		return 0;
	} else {
		// Set starting point at end of the last locked span
		pos = currentSpan->pos + currentSpan->length + 1;
		currentSpan = currentSpan->next;
	}

	printf("- starting pos %i\n", pos);

	consecutiveBlocks = 0;
	consecutiveUnknown = 0;

	gridValue = getGridValue(direction, index, pos);
	if(gridValue == BLOCK || gridValue == UNKNOWN) {
		// Look ahead
		consecutiveBlocks = 0;
		consecutiveUnknown = 0;

		for(int i = 0; i < currentSpan->length; i++) {
			gridValue = getGridValue(direction, index, pos + i);
			if(gridValue == EMPTY) {
				// Cant go here
				break;
			} else if(gridValue == BLOCK) {
				consecutiveUnknown = 0;
				consecutiveBlocks++;
			} else if(gridValue == UNKNOWN) {
				consecutiveUnknown++;
				consecutiveBlocks = 0;
			}
		}


		if(consecutiveBlocks == currentSpan->length) {

			// Check next cell
			gridValue = getGridValue(direction, index, pos + currentSpan->length);
			if(gridValue != BLOCK) {
				// This is the spot
				printf("- found 1 pos %i\n", pos);
				found = 1;
			}
			
		} else {
			// Mix of blockSpans and unknowns - check other rules

			// Check next cell
			gridValue = getGridValue(direction, index, pos + currentSpan->length);
			if(gridValue == EMPTY) {
				// Can't go any further (not 100% sure about this one)
				// printf("found 2 - index %i, pos %i\n", index, pos);
				// currentSpan->pos = pos;
				// currentSpan->locked = 1;
				// return 1;
			} else if(gridValue == UNKNOWN) {
				// We have to check the next cells to see if 

				int posPlusCount = 0;
				for(int posPlus=pos + currentSpan->length + 1; posPlus < GRID_SIZE; posPlus++) {

					gridValue = getGridValue(direction, index, posPlus);
					if(gridValue != BLOCK) {
						break;
					}

					posPlusCount++;

					if(posPlusCount > currentSpan->length) {
						// This is the spot
						printf("- found 3 pos %i\n", pos);
						found = 1;
						break;
					}
				}
			}
		}			

	}

	if(found) {
		currentSpan->pos = pos;
		currentSpan->locked = 1;

		if(currentSpan->next != NULL) {
			searchDefaultLayoutForward(direction, index, currentSpan->next, currentSpan->pos + currentSpan->length + 1);
		}

		return 1;
	}

	// No hits
	return 0;
}

int searchReverse(int direction, int index, struct GridSpan *elements)
{

	int pos = 0, gridValue = 0, consecutiveBlocks = 0, consecutiveUnknown = 0, found = 0;
	struct GridSpan *currentSpan;

	printf("%i) Search Reverse\n", index);
	
	// Find last span that is locked
	currentSpan = getLastLockedSpanReverse(elements);

	if(currentSpan == NULL) {
		// No locked span yet
		pos = GRID_SIZE - 1;
		currentSpan = getLastSpan(elements);
	} else if(currentSpan->prev == NULL) {
		// All span are locked
		return 0;
	} else {
		// Set starting point at start of the last locked span
		pos = currentSpan->pos - 1;
		currentSpan = currentSpan->prev;
	}

	printf("- starting pos %i\n", pos);

	consecutiveBlocks = 0;
	consecutiveUnknown = 0;

	gridValue = getGridValue(direction, index, pos);
	if(gridValue == BLOCK || gridValue == UNKNOWN) {
		// Look back
		consecutiveBlocks = 0;
		consecutiveUnknown = 0;

		for(int i = currentSpan->length - 1; i >= 0; i--) {
			gridValue = getGridValue(direction, index, pos + i);
			if(gridValue == EMPTY) {
				// Cant go here
				break;
			} else if(gridValue == BLOCK) {
				consecutiveUnknown = 0;
				consecutiveBlocks++;
			} else if(gridValue == UNKNOWN) {
				consecutiveUnknown++;
				consecutiveBlocks = 0;
			}
		}


		if(consecutiveBlocks == currentSpan->length) {

			// Check next cell
			gridValue = getGridValue(direction, index, pos - currentSpan->length);
			if(gridValue != BLOCK) {
				// This is the spot
				printf("- found 1 pos %i\n", pos);
				found = 1;
			}
			
		} else {
			// Mix of blocks and unknowns - check other rules

			// Check next cell
			gridValue = getGridValue(direction, index, pos - currentSpan->length);
			if(gridValue == EMPTY) {
				// Can't go any further (not 100% sure about this one)
				// printf("found 2 - index %i, pos %i\n", index, pos);
				// currentSpan->pos = pos;
				// currentSpan->locked = 1;
				// return 1;
			} else if(gridValue == UNKNOWN) {
				// We have to check the previous cells to see if known blocks are more than size of what we are checking

				int posPlusCount = 0;
				for(int posPlus=pos - (currentSpan->length + 1); posPlus >= 0; posPlus--) {

					gridValue = getGridValue(direction, index, posPlus);
					if(gridValue != BLOCK) {
						break;
					}

					posPlusCount++;

					if(posPlusCount > currentSpan->length) {
						// This is the spot
						printf("- found 3 pos %i\n", pos);
						found = 1;
						break;
					}
				}
			}
		}			

	}

	if(found) {
		currentSpan->pos = pos  - (currentSpan->length - 1);
		currentSpan->locked = 1;

		// if(currentSpan->next != NULL) {
		// 	//searchDefaultLayoutForward(direction, index, currentSpan->next, currentSpan->pos + currentSpan->length + 1);
		// }

		return 1;
	}

	// No hits
	return 0;
}


int searchMiddle(int direction, int index, struct GridSpan *elements)
{
	printf("%i) Search Middle\n", index);
	int leftPos = 0, rightPos = 0, leftGridValue = 0, rightGridValue = 0,
	cantMove = 0, len = 0, found = 0, consecutiveBlocks = 0, i = 0, j = 0, k = 0, possiblePos = 0, invalid = 0;

	struct GridSpan *testSpan = elements;

	while(testSpan != NULL) {
		i++;
		if(testSpan->locked) {
			testSpan = testSpan->next;
			continue;
		}

		printf("- test span %i len %i\n", i, testSpan->length);


		// Find left and right pos start
		leftPos = getLeftSpanStart(elements, testSpan);
		rightPos = getRightSpanStart(elements, testSpan);
		printf("- left %i, right %i\n", leftPos, rightPos);


		// Check if adjacent cells have blocks in them
		if(leftPos > 0) {
			if(getGridValue(direction, index, leftPos - 1) == BLOCK) {
				leftPos++;
			}
		}

		if(rightPos < GRID_SIZE -1) {
			if(getGridValue(direction, index, rightPos + 1) == BLOCK) {
				rightPos--;
			}
		}


		cantMove = 0;
		while ((((rightPos + 1) - leftPos) > testSpan->length) && (cantMove < 2)) {
			cantMove = 0;
			leftGridValue = getGridValue(direction, index, leftPos);
			rightGridValue = getGridValue(direction, index, rightPos);


			if(leftGridValue == EMPTY) {
				// Can't start here so move one
				leftPos++;
			} else if(leftGridValue == BLOCK) {
				// Can't move
					cantMove++;
				// gridValue = getGridValue(direction, index, leftPos + 1);
				// if(gridValue == EMPTY) {
				// 	leftPos++;
				// } else {
					
				// }
			} else if(leftGridValue == UNKNOWN) {
				// Can't move
				cantMove++;
			}


			if(rightGridValue == EMPTY) {
				// Can't end here so move one
				rightPos--;
			}  else if(rightGridValue == BLOCK) {
				// Can't move
					cantMove++;
				// gridValue = getGridValue(direction, index, rightPos - 1);
				// if(gridValue == EMPTY) {
				// 	rightPos--;
				// } else {
					
				// }
			} else if(rightGridValue == UNKNOWN) {
				// Can't move
				cantMove++;
			}
		}

		found = 0;
		len = ((rightPos + 1) - leftPos);
		if(len == 0) {
			testSpan = testSpan->next;
			continue;
		}

		leftGridValue = getGridValue(direction, index, leftPos);
		rightGridValue = getGridValue(direction, index, rightPos);
		printf("- ratio %f, left %i, right %i, len %i\n", ((float)len / (float)testSpan->length), leftPos, rightPos, len);

		if(len == testSpan->length) {

			// Check ends for test against false positive
			leftGridValue = leftPos == 0 ? EMPTY : getGridValue(direction, index, leftPos - 1);
			rightGridValue = rightPos == (GRID_SIZE - 1) ?  EMPTY : getGridValue(direction, index, rightPos + 1);

			if(leftGridValue != BLOCK && rightGridValue != BLOCK) {
				
				// Check values between left and right
				found = 1;
				for(j=0; j<len; j++) {
					if(getGridValue(direction, index, leftPos + j) == EMPTY) {
						found = 0;
						break;
					}
				}
			}
		} else if(len == testSpan->length + 1) {

			if(leftGridValue == BLOCK) {
				found = 1;
				printf("- found 1, pos %i\n", leftPos);
			} else if(rightGridValue == BLOCK) {
				found = 1;
				leftPos = rightPos - (testSpan->length - 1);
				printf("- found 2, pos %i\n", leftPos);
			}
		} 

		// Check ends
		if((testSpan->prev == NULL || testSpan->prev->locked == 1) && getGridValue(direction, index, leftPos) == BLOCK) {
			found = 1;
			printf("- found 4, pos %i\n", leftPos);
		} else if((testSpan->next == NULL || testSpan->next->locked == 1) && getGridValue(direction, index, rightPos) == BLOCK) {
			found = 1;
			leftPos = rightPos - (testSpan->length - 1);
			printf("- found 5, pos %i\n", leftPos);
		}

		if((found == 0) ) {
			// Can only fit one, try possible placements
			printf("- try possible placements\n");
			possiblePos = -1;
			for(j=leftPos; j < (rightPos + 1) - (testSpan->length - 1); j++) {

				consecutiveBlocks = 0;
				for(k=0; k<testSpan->length; k++) {

					if(getGridValue(direction, index, j + k) != EMPTY) {

						// Check perpendicular 
						struct GridSpan perpendicularSpan = getPerpendicularSpan(direction, index, j + k);
						//printf("-perpendicularSpan pos %i, start %i, len %i\n", j+k, perpendicularSpan.pos, perpendicularSpan.length);
						
						if(checkSpanFits(direction == ROW ? COL : ROW, j + k, perpendicularSpan) == 1) {
							consecutiveBlocks++;
						}
					}
				}

				printf("- pos %i, consecutiveBlocks %i\n", j, consecutiveBlocks);
				
				if((consecutiveBlocks == testSpan->length) 
					&& ((j + testSpan->length < GRID_SIZE) ? getGridValue(direction, index, j + testSpan->length) != BLOCK : 1)
					 && ((j > 0) ? getGridValue(direction, index, j - 1) != BLOCK : 1)) {

					// Check no more blocks ahead if next is locked
					invalid = 0;
					if(testSpan->next != NULL && testSpan->next->locked == 1) {
						printf("- testing for blocks ahead \n");
						for(k = j + testSpan->length; k < rightPos + 1; k++) {
							if(getGridValue(direction, index, k) == BLOCK) {
								invalid = 1;
								break;
							}
						}
					}

					// Check available space for remaining items
					// Guards against taking space from next block of similar size
					if(invalid == 0 && testSpan->next != NULL) {
						printf("- check remaining spans fit \n");
						 if(checkRemainingSpansFit(direction, index, testSpan->next, j + testSpan->length + 1) == 0) {
						 	printf("- does not fit \n");
						 	invalid = 1;
						 }
					}

					if(invalid == 0) {

						if(possiblePos != -1) {
							// Already another possibility
							possiblePos = -1;
							break;
						}
						possiblePos = j;
					}
				}
			}

			if(possiblePos != -1) {
				found = 1;
				leftPos = possiblePos;
				printf("- found 3, pos %i\n", possiblePos);
			}
		}



		if(found) {
			testSpan->pos = leftPos;
			testSpan->locked = 1;

			if(testSpan->next != NULL) {
				searchDefaultLayoutForward(direction, index, testSpan->next, testSpan->pos + testSpan->length + 1);
			} 
			if(testSpan->prev != NULL) {
				searchDefaultLayoutReverse(direction, index, testSpan->prev, testSpan->pos - 1);
			}
			return 1;
		}

		testSpan = testSpan->next;
	}

	return 0;
}

/*
*/
int searchBlocksInSpans(int direction, int index, struct GridSpan *elements)
{
	printf("%i) Search Blocks in Spans\n", index);

	struct GridSpan blockSpans[GRID_SIZE];
	struct GridSpan *currentBlockSpan = NULL;
	struct GridSpan *currentSpan = NULL;
	struct GridSpan *matchSpan = NULL;
	struct GridSpan *largestSpan = NULL;
	int pos = 0, blockSpanCount = 0, gridValue = 0, i = 0, j = 0, leftCount = 0, found = 0;

	for(pos = 0; pos < GRID_SIZE; pos++) {
		gridValue = getGridValue(direction, index, pos);

		if(currentBlockSpan == NULL) {
			
			if(gridValue == BLOCK) {
				currentBlockSpan = &blockSpans[blockSpanCount++];
				currentBlockSpan->pos = pos;
				currentBlockSpan->length = 1;
			}

			continue;
		}

		if(gridValue == BLOCK) {
			currentBlockSpan->length++;
		} else {
			currentBlockSpan = NULL;
		}
	}

	printf("- found %i blocks\n", blockSpanCount);

	// sort 
	for(i = 0; i<blockSpanCount-1; i++) {
		for(j = i + 1; j< blockSpanCount; j++) {
			if(blockSpans[j].length > blockSpans[i].length) {
				struct GridSpan tmp = blockSpans[i];
				blockSpans[i] = blockSpans[j];
				blockSpans[j] = tmp;
			}
		}
	}


	// try match block spans to spans
	for(i = 0; i<blockSpanCount; i++) {

		leftCount = 0;
		matchSpan = NULL;
		currentBlockSpan = &blockSpans[i];
		currentSpan = elements;
		largestSpan = getLargestUnlockedSpan(elements);

		

		printf("- %i pos %i len %i\n", i, currentBlockSpan->pos, currentBlockSpan->length);
		if(currentBlockSpan->length < largestSpan->length) {
			continue;
		}

		while(currentSpan != NULL) {
			if(currentSpan->pos == currentBlockSpan->pos) {
				// Block span already accounted for
				break;
			}

			if(currentSpan->locked) {
				leftCount = currentSpan->pos;
			} else if(currentSpan->length == currentBlockSpan->length) {
				// Check left
				if(leftCount <= currentBlockSpan->pos) {
					// Check right
					if((currentBlockSpan->pos + getLengthToEnd(currentSpan)) < GRID_SIZE) {
						// Possible match
						if(matchSpan != NULL) {
							// Already has another match so we cannot tell
							matchSpan = NULL;
							break;
						}
						matchSpan = currentSpan;
					} 
				}
			}

			leftCount += currentSpan->length + 1;
			currentSpan = currentSpan->next;
		}

		if(matchSpan != NULL) {
			found = 1;
			printf("- found match %i at pos %i\n", i, currentBlockSpan->pos);
			matchSpan->pos = currentBlockSpan->pos;
			matchSpan->locked = 1;
		} else {
			// Don't continue if we didn't find a match
			break;
		}
	}
	return found;
}

int searchSpansInBlocks(int direction, int index, struct GridSpan *elements)
{
	printf("%i) Search Spans in Blocks\n", index);
	struct GridSpan blockSpans[GRID_SIZE];
	int blockSpanCount = 0, currentBlockSpan = -1, pos = 0, gridValue = 0, len = 0, spanCount = 0, found = 0, endBlockSpan = 0;

	struct GridSpan *currentSpan = NULL;
	struct GridSpan *nextSpan = elements;
	
	for(pos = 0; pos < GRID_SIZE; pos++) {
		gridValue = getGridValue(direction, index, pos);

		if(currentSpan == NULL) {
			if(gridValue == BLOCK) {
				currentBlockSpan++;
				blockSpanCount++;
				blockSpans[currentBlockSpan].pos = pos;
				blockSpans[currentBlockSpan].length = 1;
				currentSpan = nextSpan;
			}

			// EMPTY, BLOCK or UNKNOWN
			continue;
		}
		
		endBlockSpan = 0;
		len = (pos - blockSpans[currentBlockSpan].pos) + 1;
		if(gridValue == EMPTY) {
			// Cannot span - end block span
			endBlockSpan = 1;
			
		} else if(gridValue == UNKNOWN) {
			
			if(len > currentSpan->length) {
				// Gone further than span could be
				endBlockSpan = 1;
			}

		} else if(gridValue == BLOCK) {

			
			if(len <= currentSpan->length) {
				// Part of this block span
				blockSpans[currentBlockSpan].length = len;
			} else {
				// Check last cell was empty or unknown
				if(getGridValue(direction, index, pos - 1) == BLOCK) {
					break;
				}

				// Larger than span can be - start a new block
				endBlockSpan = 1;

				// Replay pos to start new block span
				pos--;
			}
		}

		if(endBlockSpan) {

			if(currentSpan->next == NULL) {
				break;
			}
			nextSpan = currentSpan->next;
			currentSpan = NULL;
		}
	}

	currentSpan = elements;
	while(currentSpan != NULL) {
		spanCount++;
		currentSpan = currentSpan->next;
	}

	

	printf("- found %i blocks for %i spans\n", blockSpanCount, spanCount);

	if(blockSpanCount != spanCount) {
		return 0;
	}

	currentBlockSpan = 0;
	currentSpan = elements;
	while(currentSpan != NULL) {

		if(!currentSpan->locked) {
			if(currentSpan->length == blockSpans[currentBlockSpan].length) {
				currentSpan->locked = 1;
				currentSpan->pos = blockSpans[currentBlockSpan].pos;
				found = 1;

			}
		}
		printf("- found %i %i\n", blockSpans[currentBlockSpan].pos, blockSpans[currentBlockSpan].length);

		currentBlockSpan++;
		currentSpan = currentSpan->next;
	}

	
	return found;
}

void lockInElements(int direction, int index, struct GridSpan *elements)
{
	int pos = 0, len = 0, i = 0;
	struct GridSpan *currentSpan = elements;
	while (currentSpan != NULL) {

		if(currentSpan->locked) {
			
			pos = currentSpan->pos;
			for(len = pos + currentSpan->length; pos < len; pos++) {

				// Check
				if(getGridValue(direction, index, pos) == EMPTY) {
					printf("Error 1 %i, %i, pos %i\n", direction, index, pos);
					printGrid();
					exit(1);
				}
			
				if(direction == ROW) {
					finalGrid[index][pos] = BLOCK;
				} else {
					finalGrid[pos][index] = BLOCK;
				}
			}

			// Add required gap
			if(currentSpan->pos + currentSpan->length < GRID_SIZE) {

				i = currentSpan->pos + currentSpan->length;
				len = i + 1;

				if(currentSpan->next == NULL) {
					len = GRID_SIZE;
				} else if(currentSpan->next->locked == 1) {
					len = currentSpan->pos;
				}

				for(;i<len; i++) {
					// Check
					if(getGridValue(direction, index, i) == BLOCK) {
						printf("Error 2 %i, %i, pos %i\n", direction, index, i);
						printGrid();
						exit(1);
					}

					if(direction == ROW) {
						finalGrid[index][i] = EMPTY;
					} else {
						finalGrid[i][index] = EMPTY;
					}
				}
			}
			if(currentSpan->pos > 0) {

				i = currentSpan->pos - 1;
				len = i - 1;
				if(currentSpan->prev == NULL) {
					len = -1;
				} else if(currentSpan->prev->locked == 1) {
					len = currentSpan->prev->pos + (currentSpan->prev->length - 1);
				}

				for(;i>len; i--) {
					// Check
					if(getGridValue(direction, index, i ) == BLOCK) {
						printf("Error 3 %i, %i, pos %i\n", direction, index, i);
						printGrid();
						exit(1);
					}

					if(direction == ROW) {
						finalGrid[index][i] = EMPTY;
					} else {
						finalGrid[i][index] = EMPTY;
					}
				}
			}
		}

		currentSpan = currentSpan->next;
	}
}

void fillInBlanks(int direction, int index, struct GridSpan *elements)
{
	printf("%i) Fill in blanks\n", index);
	int i = 0, k = 0, len = 0, distance = 0, gridValue = 0;
	struct GridSpan *span = NULL;

	// Find distance to first empty
	span = elements;
	len = GRID_SIZE;
	distance = 0;
	for( i = 0; i < len; i++ ) {
		gridValue = getGridValue(direction, index, i);
		if(gridValue == EMPTY) {
			distance = i;
			break;
		} else if(gridValue == BLOCK) {
			// Blocks found before empty
			distance = 0;
			break;
		}
	}

	if(distance > 0 && span->length > distance) {
		// Can't fit first span so fill with empties
		for( i = 0; i < distance; i++ ) {
			if(direction == ROW) {
				finalGrid[index][i] = EMPTY;
			} else {
				finalGrid[i][index] = EMPTY;
			}
		}
	}

	// Find distance to last empty
	span = getLastSpan(elements);
	len = GRID_SIZE;
	distance = 0;
	for( i = 0; i < len; i++ ) {
		gridValue = getGridValue(direction, index, (GRID_SIZE - 1) - i);
		if(gridValue == EMPTY) {
			distance = i;
			break;
		} else if(gridValue == BLOCK) {
			// Blocks found before empty
			distance = 0;
			break;
		}
	}

	if(distance > 0 && span->length > distance) {
		// Can't fit last span so fill with empties
		for( i = 0; i < distance; i++ ) {
			if(direction == ROW) {
				finalGrid[index][(GRID_SIZE - 1) - i] = EMPTY;
			} else {
				finalGrid[(GRID_SIZE - 1) - i][index] = EMPTY;
			}
		}
	}

	// Fill gaps that aren't large enough for a span (forward)
	span = elements;
	while (span != NULL) {

		if(span->locked == 1 && span->next != NULL && span->next->locked == 0) {

			// Collect blocks of unknown surrounded by empties
			distance = -1;
			for( i = span->pos + span->length; i < GRID_SIZE; i++) {
				gridValue = getGridValue(direction, index, i);
				if(gridValue == UNKNOWN) {
					
					// Only count if we started with an empty
					if(distance > -1) {
						distance++;
					}
					
				}  else if(gridValue == BLOCK) {
					// Up against block
					break;
				} else if(gridValue == EMPTY) {
					if(distance > 0) {
						printf("- found block of unknown pos %i, len %i\n", i - distance,  distance);

						// See if next span will fit 
						if(span->next->length > distance) {
							for( k = i - distance; k < i; k++) {
								if(direction == ROW) {
									finalGrid[index][k] = EMPTY;
								} else {
									finalGrid[k][index] = EMPTY;
								}
							}
						}
						break;
					}
					distance = 0;
				}
			}

		}

		span = span->next;
	}

	// Fill gaps that aren't large enough for a span (reverse)
	span = getLastSpan(elements);
	while (span != NULL) {

		if(span->locked == 1 && span->prev != NULL && span->prev->locked == 0) {

			// Collect blocks of unknown surrounded by empties
			distance = -1;
			for( i = span->pos - 1; i > -1; i--) {

				
				gridValue = getGridValue(direction, index, i);
				if(gridValue == UNKNOWN) {
					
					// Only count if we started with an empty
					if(distance > -1) {
						distance++;
					}
					
				}  else if(gridValue == BLOCK) {
					// Up against block
					break;
				} else if(gridValue == EMPTY) {
					if(distance > 0) {
						printf("- found block of unknown pos %i, len %i\n", i + 1,  distance);

						// See if any spans will fit in this 
						if(span->prev->length > distance) {
							for( k = i + 1; k < i + distance + 1; k++) {
								if(direction == ROW) {
									finalGrid[index][k] = EMPTY;
								} else {
									finalGrid[k][index] = EMPTY;
								}
							}
						}
						break;
					}
					distance = 0;
				}
			}
		}

		span = span->prev;
	}


	// Fill gaps between 0 and first span
	span = elements;
	if(span->locked == 0 && span->next->locked == 1) {

		// Find last block
		distance = -1;
		for(i = 0; i < span->next->pos - 1; i++ ) {
			gridValue = getGridValue(direction, index, i);
			if(gridValue == BLOCK) {
				distance = i;
			} else if(gridValue == EMPTY) {
				break;
			}
		}

		distance -= (span->length - 1);
		if(distance > 0) {
			for( i = 0; i< distance; i++) {
				if(direction == ROW) {
					finalGrid[index][i] = EMPTY;
				} else {
					finalGrid[i][index] = EMPTY;
				}
			}
			printf("- found empty pos %i, len %i\n", 0, distance);
		}
	}



}